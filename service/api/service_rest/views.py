from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import TechnicianDetailEncoder, AppointmentDetailEncoder
from .models import Technician, Appointment


@require_http_methods(["GET","POST"])
def TechnicianList(request):
    if request.method == "GET":
        try:
            technicians = Technician.objects.all()
            return JsonResponse(
                {"technicians":technicians},
                encoder=TechnicianDetailEncoder,
                )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid technician id"}, status=400)
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False
        )
    
    
@require_http_methods(["GET","POST","DELETE"])
def TechnicianDetail(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            technician_data = {
                "id": technician.id,
                "first_name": technician.first_name,
                "last_name": technician.last_name,
                "employee_id": technician.employee_id,
            }
            return JsonResponse(technician_data, safe=False)
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid technician id"}, status=400)
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(employee_id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def AppointmentList(request):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.all()
            return JsonResponse(
                {"appointments": appointments},
                encoder=AppointmentDetailEncoder,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid appointment id"}, status=400)
    else:
        content = json.loads(request.body)
        try:
            tech_id = content['technician']
            technician = Technician.objects.get(employee_id=tech_id)
            content["technician"] = technician
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Invalid technician id"}, status=400)

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False
        )
    
    
@require_http_methods(["DELETE", "GET", "PUT"])
def AppointmentDetail(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder = AppointmentDetailEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Invalid appointment id"}, status=400)
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    
    
@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.finish()
        return JsonResponse({"status": appointment.status, "message": "success!"}, status=200)
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Invalid appointment id"}, status=400)
    

@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.cancel()
        return JsonResponse({"status": appointment.status, "message": "success!"}, status=200)
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Invalid appointment id"}, status=400)

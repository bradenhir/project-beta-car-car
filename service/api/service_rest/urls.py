from django.urls import path
from .views import AppointmentList, TechnicianList, AppointmentDetail, TechnicianDetail, api_cancel_appointment, api_finish_appointment


urlpatterns = [
    path("appointments/", AppointmentList, name="show_appointment_list"),
    path("technicians/", TechnicianList, name="show_technician_list"),
    path("appointments/<int:pk>/",AppointmentDetail, name="show_appointment_detail"),
    path("technicians/<int:pk>/",TechnicianDetail, name="show_Technician_detail"),
    path(
        "appointments/<int:pk>/finished/",
        api_finish_appointment,
        name="finished_appointment",
    ),
    path(
        "appointments/<int:pk>/cancel/",
        api_cancel_appointment,
        name="canceled_appointment",
    ),
]

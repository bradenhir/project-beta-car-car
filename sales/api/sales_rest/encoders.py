from common.json import ModelEncoder
from .models import Customer, Salesperson, AutomobileVO, Sale


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]


class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "customer",
        "salesperson",
        "automobile"
    ]
    encoders = {
        "salesperson": SalespersonListEncoder(),
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerListEncoder()
    }

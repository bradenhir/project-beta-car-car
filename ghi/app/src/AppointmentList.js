import React, { useEffect, useState } from 'react';
import AppointmentCard from './AppointmentCard';


function AppointmentList() {
  const [appointmentCards, setAppointmentCards] = useState([]);
  const deleteAppointment = async (index, vin) => {
    const appointmentURL = 'http://localhost:8080/api/appointments/' + vin;
    const fetchConfig = {
      method: "delete",
    };

    const response = await fetch(appointmentURL, fetchConfig);
    if (response.ok) {
      const updatedAppointmentCards = appointmentCards.filter((_, i) => i !== index);
      setAppointmentCards(updatedAppointmentCards);
    }
  };

  const updateAppointmentStatus = async (appointmentId, newStatus) => {
    const appointmentURL = 'http://localhost:8080/api/appointments/' + appointmentId;
    const fetchConfig = {
      method: "put",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ status: newStatus }),
    };

    const response = await fetch(appointmentURL, fetchConfig);
    if (response.ok) {
      const updatedAppointmentCards = appointmentCards.map(appointmentCard => {
        if (appointmentCard.id === appointmentId) {
          return { ...appointmentCard, status: newStatus };
        }
        return appointmentCard;
      });
      setAppointmentCards(updatedAppointmentCards);
    }
  };

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/appointments/';

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAppointmentCards(data.appointments);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container mt-4">
      <h2>Service Appointments</h2>
      <div className="row row-cols-3">
        {appointmentCards.map((appointmentCard, index) => {
          return (
            <AppointmentCard
              key={index}
              index={index}
              appointment={appointmentCard}
              updateAppointmentStatus={updateAppointmentStatus}
              deleteAppointment={() => deleteAppointment(index, appointmentCard.vin)}
            />
          );
        })}
      </div>
    </div>
  );
}

export default AppointmentList;

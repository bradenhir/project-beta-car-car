import React, { useEffect, useState } from 'react';


function AutomobileForm() {
  const [formData, setFormData] = useState({
    color: '',
    year: '',
    vin: '',
    model_id: '',
  });

  const [models, setModels] = useState([]);
  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setFormData({
          color: '',
          year: '',
          vin: '',
          model_id: '',
        });
        alert("Automobile created");
      } else {
        alert("Failed to add the Automobile!");
      }
    } catch (e) {
      console.error("An error occurred during the fetch operation:", e);
      alert("Error occurred!");
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  }

  useEffect(() => {
    async function fetchModels() {
      try {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (!response.ok) {
          alert("Bad response while fetching models!");
        } else {
          const data = await response.json();
          setModels(data.models);
        }
      } catch (e) {
        console.error("An error occurred during the fetch operation:", e);
        alert("Error occurred while fetching models!");
      }
    }

    fetchModels();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add an automobile</h1>
          <form onSubmit={handleSubmit} id="add-hat-form">
          <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={formData.color} />
              <label htmlFor="color">Color</label>
            </div>
          <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Year" required type="number" name="year" id="year" className="form-control" value={formData.year} />
            <label htmlFor="year">Year</label>
          </div>
          <div className="form-floating">
              <input onChange={handleFormChange} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" value={formData.vin} />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="mb-3">
              <label htmlFor="model_id"></label>
              <select onChange={handleFormChange} name="model_id" id="model_id" className="form-select" value={formData.model_id}>
                <option value="">Choose a model</option>
                {models.map(model_id => {
                  return (
                    <option key={model_id.id} value={model_id.id}>{model_id.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default AutomobileForm;

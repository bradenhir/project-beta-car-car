function MainPage() {

  const gifStyle = {
    position: 'relative',
    width: '100%',
    height: '100vh',
  };
  const carmoveStyle = {
    width: '100%',
    height: '100%',
    objectFit: 'cover',
  }

  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
      </div>
      <div style={gifStyle}>
      <img src="https://cdn.dribbble.com/users/227692/screenshots/2106943/flp-001.gif" style={carmoveStyle} />
      </div>
    </div>

  );
}

export default MainPage;

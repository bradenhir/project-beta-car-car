import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import MainPage from './MainPage';
import ServiceHistoryPage from './ServiceHistoryPage';
import VehicleModelPage from './VehicleModelPage';
import AutomobileForm from './AutomobileForm';
import AutomobilePage from './AutomobilePage';
import SalespeopleList from './SalespersonList';
import SalespersonForm from './SalespersonForm';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SaleList from './SalesList';
import SaleForm from './SalesForm';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import VmodelForm from './VehicleModelForm';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import AppointmentList from './AppointmentList';
import AppointmentForm from './AppointmentForm';
import SalesHistoryList from './SalesHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/servicehistory" element={<ServiceHistoryPage />} />
          <Route path="/vehiclemodels" element={<VehicleModelPage />} />
          <Route path="/automobiles/new" element={<AutomobileForm />} />
          <Route path="/automobiles" element={<AutomobilePage />} />
          <Route path="/" element={<MainPage />} />

          <Route path="salespeople">
            <Route index element={<SalespeopleList />} />
            <Route path="create" element={<SalespersonForm />} />
          </Route>

          <Route path="customers">
            <Route index element={<CustomerList />} />
            <Route path="create" element={<CustomerForm />} />
          </Route>

          <Route path="sales">
            <Route index element={<SaleList />} />
            <Route path="create" element={<SaleForm />} />
            <Route path="history" element={<SalesHistoryList />} />
          </Route>

          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="create" element={<ManufacturerForm />} />
          </Route>

          <Route path="models">
            <Route index element={<VehicleModelPage />} />
            <Route path="create" element={<VmodelForm />} />
          </Route>

          <Route path="technicians">
            <Route index element={<TechnicianList />} />
            <Route path="add" element={<TechnicianForm />} />
          </Route>

          <Route path="appointments">
            <Route index element={<AppointmentList />} />
            <Route path="add" element={<AppointmentForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

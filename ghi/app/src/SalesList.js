import React, { useEffect, useState } from 'react';
import SaleCard from './SalesCard';


function SaleList() {
  const [saleCards, setSaleCards] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/sales/';

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSaleCards(data.sales);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container mt-4">
      <h2>Sales</h2>
      <div className="row row-cols-3">
        {saleCards.map((saleCard) => {
          return (
            <SaleCard
              key={saleCard.id}
              sale={saleCard}
            />
          );
        })}
      </div>
    </div>
  )
}
export default SaleList;

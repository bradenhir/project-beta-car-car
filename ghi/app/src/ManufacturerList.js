import React, { useEffect, useState } from 'react';
import ManufacturerCard from './ManufacturerCard';

function ManufacturerList() {
  const [manufacturerCards, setManufacturerCards] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers';

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturerCards(data.manufacturers);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container mt-4">
      <h2>Manufacturers</h2>
      <div className="row row-cols-3">
        {manufacturerCards.map((manufacturerCard, index) => {
          return (
            <ManufacturerCard
              key={index}
              index={index}
              manufacturer={manufacturerCard}
            />
          );
        })}
      </div>
    </div>

  );
}

export default ManufacturerList;

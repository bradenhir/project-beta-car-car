import React, { useState } from 'react';


function ManufacturerForm() {
    const [nameName, setName] = useState('');

    const handleSubmit = async (event) => {
      event.preventDefault();

      const data = {
        name: nameName,
      };

      const manURL = 'http://localhost:8100/api/manufacturers/';
      const fetchConfig = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

          const response = await fetch(manURL, fetchConfig);
          if (response.ok) {
            const addManufacturer = await response.json();

            setName('')
          }
          }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);

    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={nameName} placeholder="Name" required type="text" name="Name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )

    }
export default ManufacturerForm;

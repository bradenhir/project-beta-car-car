import React, { useState, useEffect } from 'react';


function AutomobilePage({ color, onDelete, year, model, vin, sold, manufacturer }) {
  const handleDelete = () => {
    const confirmDelete = window.confirm('Are you sure you want to delete this automobile?');
    if (confirmDelete) {
      onDelete();
    }
  };

  const soldDisplayValue = sold ? 'Yes' : 'No';
  return (
    <tr>
      <td>{vin}</td>
      <td>{color}</td>
      <td>{year}</td>
      <td>{model}</td>
      <td>{manufacturer}</td>
      <td>{soldDisplayValue}</td>
      <td>
        <button onClick={handleDelete}type="button" className="btn btn-danger">Delete</button>
      </td>
    </tr>
  );
}

function AutomobilePageContainer() {
  const [automobilesData, setAutomobilesData] = useState([]);

  useEffect(() => {
    const fetchAutomobileData = async () => {
      const url = 'http://localhost:8100/api/automobiles/';
      try {
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error('Bad response!');
        } else {
          const data = await response.json();
          const automobile = data.autos.map(async (automobile) => {
            const detailUrl = `http://localhost:8100/api/automobiles/${automobile.vin}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              return {
                id: automobile.id,
                color: details.color,
                year: details.year,
                vin: details.vin,
                model: details.model.name,
                manufacturer: details.model.manufacturer.name,
                sold: details.sold,
              };
            } else {
              throw new Error('Error occurred, could not get details!');
            }
          });
          Promise.all(automobile)
            .then((automobileDetails) => setAutomobilesData(automobileDetails))
            .catch((error) => console.error(error));
        }
      } catch (e) {
        console.error(e);
        alert('Error was raised!');
      }
    };

    fetchAutomobileData();
  }, []);

  const handleDeleteAutomobile = async (automobileIndex) => {
    const automobileToDelete = automobilesData[automobileIndex];
    const deleteUrl = `http://localhost:8100/api/automobiles/${automobileToDelete.vin}`;
    try {
      const response = await fetch(deleteUrl, {
        method: 'DELETE',
      });
      
      if (response.ok) {
        setAutomobilesData((prevAutomobiles) => prevAutomobiles.filter((_, index) => index !== automobileIndex));
      } else {
        throw new Error('Error occurred while deleting the automobile!');
      }
    } catch (e) {
      console.error(e);
      alert('Error occurred while deleting the automobile!');
    }
  };

  return (
    <div>
      <h1>Automobiles</h1>
      <table id="automobileContainer" className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {automobilesData.map((automobile, index) => (
            <AutomobilePage
              key={index}
              color={automobile.color}
              year={automobile.year}
              vin={automobile.vin}
              model={automobile.model}
              manufacturer={automobile.manufacturer}
              sold={automobile.sold}
              onDelete={() => handleDeleteAutomobile(index)}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AutomobilePageContainer;

import React from 'react';


function ManufacturerCard(props) {
  if (props.manufacturer === undefined) {
    return null;
  }
  const cardStyle = {
    backgroundColor: '#198754',
    padding: '10px',
    borderRadius: '8px',
    color: 'white'
  };
  return (
    <div className="col">
      <div key={props.manufacturer.name} className="card mb-3 shadow">
        <div className="card-body" style={cardStyle}>
          <h5 className="card-title">Name: {props.manufacturer.name}</h5>
        </div>
      </div>
    </div>
  );
}

export default ManufacturerCard;

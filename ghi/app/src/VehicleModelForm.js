import React, { useState, useEffect } from 'react';


function VmodelForm() {
    const [thenameName, setThename] = useState('');
    const [picurlName, setPicurl] = useState('');
    const [manufacName, setManufac] = useState('');
    const [manufacs, setManufacs] = useState([])
    const handleSubmit = async (event) => {
      event.preventDefault();

      const data = {
        name: thenameName,
        picture_url: picurlName,
        manufacturer_id: manufacName,
      };

      const modelURL = 'http://localhost:8100/api/models/';
      const fetchConfig = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

        const response = await fetch(modelURL, fetchConfig);
          if (response.ok) {
            const addvehiclemodel = await response.json();

            setThename('');
            setPicurl('');
            setManufac('');
            setManufacs([]);
          }
          }

    const handleThenameChange = (event) => {
        const value = event.target.value;
        setThename(value);

    }

    const handlePicurlChange = (event) => {
        const value = event.target.value;
        setPicurl(value);

    }

    const handleManufacChange = (event) => {
        const value = event.target.value;
        setManufac(value);

    }
    const fetchData = async () => {
        const vmodurl = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(vmodurl);
        if (response.ok) {
          const data = await response.json();
          setManufacs(data.manufacturers)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a vehicle model</h1>
            <form onSubmit={handleSubmit} id="create-vehiclemodel-form">
              <div className="form-floating mb-3">
                <input onChange={handleThenameChange} value={thenameName} placeholder="Model name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Model</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePicurlChange} value={picurlName} placeholder="Picture url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="pictureUrl">Picture URL</label>
              </div>
              <div className="mb-3">
                  <select
                    onChange={handleManufacChange}
                    value={manufacName}
                    required
                    name="manufac"
                    id="manufac"
                    className="form-select"
                  >
                <option value="">Choose a Manufacturer</option>
                {manufacs.map((manufac) => (
                  <option value={manufac.id} key={manufac.id}>
                    Name: {manufac.name}
                  </option>
                ))}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
    }

export default VmodelForm;

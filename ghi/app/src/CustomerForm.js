import React, { useState } from 'react';


function CustomerForm() {
    const [firstName, setFirst] = useState('');
    const [lastName, setLast] = useState('');
    const [addressName, setAddress] = useState('');
    const [phonenumberName, setPhonenumber] = useState('');

    const handleSubmit = async (event) => {
      event.preventDefault();

      const data = {
        first_name: firstName,
        last_name: lastName,
        address: addressName,
        phone_number: phonenumberName,
      };

      const customerURL = 'http://localhost:8090/api/customers/';
      const fetchConfig = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

          const response = await fetch(customerURL, fetchConfig);
          if (response.ok) {
            const addCustomer = await response.json();

            setFirst('');
            setLast('');
            setAddress('');
            setPhonenumber('');
          }
          }

    const handleFirstChange = (event) => {
        const value = event.target.value;
        setFirst(value);

    }

    const handleLastChange = (event) => {
        const value = event.target.value;
        setLast(value);

    }

    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);

    }

    const handlePhonenumberChange = (event) => {
        const value = event.target.value;
        setPhonenumber(value);

    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Customer</h1>
            <form onSubmit={handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input onChange={handleFirstChange} value={firstName} placeholder="First name" required type="text" name="first_name" id="first_name" className="form-control"/>
                <label htmlFor="first_name">First name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleLastChange} value={lastName} placeholder="Last name" required type="text" name="last_name" id="last_name" className="form-control"/>
                <label htmlFor="last_name">Last name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleAddressChange} value={addressName} placeholder="address" required type="text" name="address" id="address" className="form-control"/>
                <label htmlFor="address">Address</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePhonenumberChange} value={phonenumberName} placeholder="phone number" required type="text" name="phone_number" id="phone_number" className="form-control"/>
                <label htmlFor="phone_number">Phone number</label>
              </div>
              <div className="mb-3">
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
  }

export default CustomerForm;

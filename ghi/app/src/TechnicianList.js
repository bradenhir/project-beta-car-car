import React, { useEffect, useState } from 'react';
import TechnicianCard from './TechnicianCard';


function TechnicianList() {
  const [technicianCards, setTechnicianCards] = useState([]);

  const deleteTechnician = async (index, pk) => {
    const technicianURL = 'http://localhost:8080/api/technicians/' + pk;
    const fetchConfig = {
      method: "delete",
    };

    const response = await fetch(technicianURL, fetchConfig);
    if (response.ok) {
      const updatedTechnicianCards = technicianCards.filter((_, i) => i !== index);
      setTechnicianCards(updatedTechnicianCards);
    }
  };

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechnicianCards(data.technicians);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container mt-4">
      <h2>A list of technicians</h2>
      <div className="row row-cols-3">
        {technicianCards.map((technicianCard, index) => {
          return (
            <TechnicianCard
              key={index}
              index={index}
              technician={technicianCard}
              deleteTechnician={() => deleteTechnician(index, technicianCard.employee_id)}
            />
          );
        })}
      </div>
    </div>
  );
}

export default TechnicianList;
